// backend/testConnection.js
const mongoose = require('mongoose');

const mongoUrl = process.env.MONGO_URL || 'mongodb://mongodb:27017/taskmanager';

mongoose.connect(mongoUrl, { useNewUrlParser: true, useUnifiedTopology: true })
  .then(() => {
    console.log('Successfully connected to MongoDB');
    process.exit(0);
  })
  .catch(err => {
    console.error('Error connecting to MongoDB', err);
    process.exit(1);
  });

