# Task Manager

## Description

Task Manager est une application web pour gérer des tâches.

## Installation

Clonez le dépôt:

```bash
git clone <repository_url>
cd taskmanager
```

### Backend

1. Accédez au répertoire backend:

```bash
cd backend
```

2. Installez les dépendances:

```bash
npm install
```

3. Lancez le serveur backend:

```bash
npm start
```

Le serveur backend devrait maintenant être accessible sur `http://localhost:3001`.

### Frontend

1. Accédez au répertoire frontend:

```bash
cd ../frontend
```

2. Installez les dépendances:

```bash
npm install
```

3. Lancez le serveur de développement frontend:

```bash
npm start
```

Le frontend devrait maintenant être accessible sur `http://localhost:3000`.

## Exécution des Tests

### Tests End-to-End avec Cypress

1. Assurez-vous que les serveurs backend et frontend sont en cours d'exécution dans des terminaux séparés.

2. Installez Cypress (si ce n'est pas déjà fait):

```bash
npm install cypress --save-dev
```

3. Lancez Cypress:

```bash
npx cypress open
```

4. Dans la fenêtre Cypress qui s'ouvre, cliquez sur `tasks.spec.cy.js` sous la section `Integration tests` pour exécuter les tests end-to-end.

#### Tests End-to-End avec Docker

```bash
docker-compose up -d --build
docker-compose exec cypress npx cypress run --spec "cypress/e2e/tasks.spec.cy.js"
```

### Tests Backend

Pour exécuter les tests backend:

```bash
cd backend
npm test
```

### Tests Frontend

Pour exécuter les tests frontend:

```bash
cd frontend
docker-compose exec cypress npx cypress run --spec "cypress/integration/tasks_mocked.spec.cy.js"
```


## Notes Additionnelles

Ajoutez ici toute note ou information supplémentaire pertinente sur le projet.
