import globals from "globals";
import pluginJs from "@eslint/js";
import pluginReactConfig from "eslint-plugin-react/configs/recommended.js";

export default [
  { files: ["**/*.{js,mjs,cjs,jsx}"] },
  {
    languageOptions:
    {
      globals:
      {
        ...globals.browser,
        ...globals.node,
        React: "readonly",
        test: "readonly",
        expect: "readonly"
      }
    }
  },
  pluginJs.configs.recommended,
  pluginReactConfig,
  {
    languageOptions: {
      globals: {
        describe: false,
        beforeEach: false,
        cy: false,
        it: false
      }
    }
  }
];
