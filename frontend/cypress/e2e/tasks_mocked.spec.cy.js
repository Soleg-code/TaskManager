describe('Task Manager with Mocked API', () => {
  beforeEach(() => {
    cy.intercept('GET', '/api/tasks', { fixture: 'tasks.json' }).as('getTasks');
    cy.visit('/');
  });

  it('should display mocked tasks', () => {
    cy.wait('@getTasks');
    cy.contains('li', 'Mocked Task 1');
    cy.contains('li', 'Mocked Task 2');
  });

  it('should allow adding a new task with mocked response', () => {
    cy.intercept('POST', '/api/tasks', {
      statusCode: 201,
      body: { _id: '123', title: 'New Mocked Task', completed: false },
    }).as('addTask');

    cy.get('input[placeholder="Add a new task"]').type('New Mocked Task');
    cy.contains('button', 'Add Task').click();
    cy.wait('@addTask');

    cy.contains('li', 'New Mocked Task');
  });
});
